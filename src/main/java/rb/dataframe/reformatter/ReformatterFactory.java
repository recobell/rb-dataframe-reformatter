package rb.dataframe.reformatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ReformatterFactory {

    @Value("${recobell.dataframe.reformatter.className}")
    private String reformatterClassName;

    ReflectedFormatter getInstance()
        throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        return new ReflectedFormatter().loadReformatterClass(reformatterClassName).loadReformatterMethod("reformat");
    }

}
