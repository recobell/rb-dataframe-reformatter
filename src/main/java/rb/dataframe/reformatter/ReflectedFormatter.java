package rb.dataframe.reformatter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import rb.dataframe.reformatter.formatter.Reformatter;

public class ReflectedFormatter {

    private Class<Reformatter> claz;
    private Method md;
    private Object tmpClaz;

    @SuppressWarnings("unchecked")
    public ReflectedFormatter loadReformatterClass(String reformatterClassName)
        throws ClassNotFoundException

    {
        this.claz = (Class<Reformatter>) Class.forName(reformatterClassName);
        return this;
    }

    public ReflectedFormatter loadReformatterMethod(String reformatterMethodName)
        throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException
    {
        this.md = claz.getMethod(reformatterMethodName, String.class, int.class);
        tmpClaz = claz.newInstance();
        return this;
    }

    public String reformat(String line, int idx)
        throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        return (String) md.invoke(tmpClaz, line, idx);
    }

}
