package rb.dataframe.reformatter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import rb.dataframe.reformatter.formatter.Reformatter;

public class ReflectedFilter {

    private Class<Reformatter> claz;
    private Method md;
    private Object tmpClaz;

    @SuppressWarnings("unchecked")
    public ReflectedFilter loadFilterClass(String filterClassName)
        throws ClassNotFoundException

    {
        this.claz = (Class<Reformatter>) Class.forName(filterClassName);
        return this;
    }

    public ReflectedFilter loadFilterMethod(String filterMethodName)
        throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException
    {
        this.md = claz.getMethod(filterMethodName, String.class);
        tmpClaz = claz.newInstance();
        return this;
    }

    public boolean doFilter(String line)
        throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        return (boolean) md.invoke(tmpClaz, line);
    }

}
