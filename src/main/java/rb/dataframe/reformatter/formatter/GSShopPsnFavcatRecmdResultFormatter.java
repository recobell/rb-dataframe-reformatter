package rb.dataframe.reformatter.formatter;

/**
 * 
 * @author jackyun
 * @since 2015. 12. 30.
 */
public class GSShopPsnFavcatRecmdResultFormatter
    implements Reformatter
{

    public String reformat(String line, int idx) {
        String[] splittedLine = line.split(",");

        String pcid = splittedLine[0];
        String item_id = splittedLine[1];
        String score = splittedLine[2];
        String rating = "0";
        String oivr = "0";

        return String.join("\t", Integer.toString(idx), pcid, item_id, score, rating, oivr);

    }

}
