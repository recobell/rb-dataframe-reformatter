package rb.dataframe.reformatter.formatter;

/**
 * 
 * @author 	jackyun
 * @since	2016. 1. 8.
 */
public interface Reformatter {
    public String reformat(String line, int idx);
}
