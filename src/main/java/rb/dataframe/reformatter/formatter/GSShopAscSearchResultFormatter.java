package rb.dataframe.reformatter.formatter;

import java.util.Base64;
/**
 * 
 * @author jackyun
 * @since 2015. 12. 30.
 */
public class GSShopAscSearchResultFormatter
    implements Reformatter
{

    public String reformat(String line, int idx) {
        String[] splittedLine = line.split(",");

        String searchTerm = splittedLine[0].replaceAll("\"", "").replaceAll("\\s+","");
        String item_id = splittedLine[1];
        String score = splittedLine[2];
        String rating = "0";
        String oivr = "0";

        return String.join("\t", decodeBase64(searchTerm), item_id, score, rating, oivr);

    }

    private String decodeBase64(String word) {

        word = word.replaceAll("\"\"", "").trim();

        return new String(Base64.getDecoder().decode(word));
    }

    
}
