package rb.dataframe.reformatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FilterFactory {

    @Value("${recobell.dataframe.filter.className}")
    private String filterClassName;

    public ReflectedFilter getInstance()
        throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        return new ReflectedFilter().loadFilterClass(filterClassName).loadFilterMethod("doFilter");
    }

}
