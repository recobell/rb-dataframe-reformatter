package rb.dataframe.reformatter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

@SpringBootApplication
public class RbDataframeRefomatter
    implements CommandLineRunner
{

    public static void main(String[] args) {
        SpringApplication.run(RbDataframeRefomatter.class, args);
    }

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${aws.accessKeyId}")
    private String accessKeyId;

    @Value("${aws.secretKey}")
    private String secretKey;

    @Value("${recobell.rec.results.s3.endpoint}")
    private String s3Endpoint;

    @Value("${recobell.rec.results.s3.maxConnection}")
    private int maxConnections;

    @Value("${recobell.rec.results.s3.bucket}")
    private String recS3Bucket;

    @Value("${recobell.file.encoding.converter.sourceEncoding}")
    private String sourceEncoding;

    @Value("${recobell.file.encoding.converter.targetEncoding}")
    private String targetEncoding;

    @Value("${recobell.file.encoding.converter.sourcefilePattern}")
    private String sourcefilesPattern;

    @Value("${recobell.file.encoding.converter.targetBucket}")
    private String targetBucket;

    @Value("${recobell.file.encoding.converter.targetfilePath}")
    private String targetPath;

    @Value("${recobell.file.encoding.converter.cuid}")
    private String cuid;

    @Value("${recobell.product.getter.exportTableSeparator}")
    private String exportTableSeparator;

    @Value("${recobell.product.getter.exportFileType}")
    private String exportFileType;

    @Bean
    public AmazonS3 amazonS3() {
        ClientConfiguration awsConfiguration = new ClientConfiguration();
        awsConfiguration.setMaxConnections(maxConnections);
        AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKeyId, secretKey), awsConfiguration);
        s3Client.setEndpoint(s3Endpoint);
        return s3Client;
    }

    @Autowired
    AmazonS3 s3Client;

    @Autowired
    ReformatterFactory reformatterFactory;

    @Autowired
    FilterFactory filterFactory;

    /**
     * 
     * @author jackyun
     * @since 2015. 11. 6.
     * @param arg0
     * @throws Exception
     */
    @Override
    public void run(String... arg0)
        throws Exception
    {
        getSourceFilesKey(cuid, sourcefilesPatternFormatReader(sourcefilesPattern))
            .forEach(key ->
        {
                try {

                    String sourceFileName = parseSourceFileName(key.split("/"));

                    File convertedFile = convertDataFrame(key);

                    String targetFileName = modifyFileExtension(sourceFileName);

                    export(convertedFile, createTargetPath(cuid, targetPath, targetFileName));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

    }

    private List<String> getSourceFilesKey(String cuid, String sourcefilesPattern) {

        List<String> sourceFileKeys = new ArrayList<>();
        s3Client.listObjects(recS3Bucket, createFilePattern(cuid, sourcefilesPattern))
            .getObjectSummaries()
            .parallelStream()
            .forEach(s3ObjSummary -> sourceFileKeys.add(s3ObjSummary.getKey()));

        sourceFileKeys.forEach(key -> logger.info("target sourceFile key {}", key));
        if (sourceFileKeys.size() ==0)logger.info("there is no sourceFile check, your source file pattern {}", sourcefilesPattern);
        return sourceFileKeys;

    }

    private File convertDataFrame(String key)
        throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException, ClassNotFoundException
    {
        logger.info("try to convert {} table format", key);

        File tempFile = convertTableFormat(key);

        logger.info("success to convert {} table format", key);
        return tempFile;
    }

    private InputStream getInputStream(String s3ObjKey) {
        return s3Client.getObject(recS3Bucket, s3ObjKey).getObjectContent();
    }

    private String modifyFileExtension(String filePath) {
        String baseFileName = filePath.split("\\.")[0];
        return String.join(".", baseFileName, exportTableSeparator, exportFileType);
    }

    private void export(File tempFile, String convertedFileKey) {
        logger.info("try to upload {} to S3 ", convertedFileKey);
        s3Client.putObject(targetBucket, convertedFileKey, tempFile);
        logger.info("upload to S3 complete for {}", convertedFileKey);
    }

    private String createFilePattern(String cuid, String filePattern) {
        return String.join("/", cuid, filePattern);
    }

    private File convertTableFormat(String key)
        throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException, ClassNotFoundException
    {
        ReflectedFormatter formatter = reformatterFactory.getInstance();
        ReflectedFilter filter = filterFactory.getInstance();
        File tempFile = File.createTempFile(UUID.randomUUID().toString(), ".tmp");

        try (InputStream is = getInputStream(key);
            GZIPInputStream gis = new GZIPInputStream(is);
            InputStreamReader isr = new InputStreamReader(gis, sourceEncoding);
            BufferedReader br = new BufferedReader(isr);
            FileOutputStream fos = new FileOutputStream(tempFile);
            GZIPOutputStream gzos = new GZIPOutputStream(fos);)
        {
            AtomicInteger idx = new AtomicInteger();
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                //to pass column name line

                if (idx.getAndIncrement() == 0) continue;

                if (!filter.doFilter(line)) gzos.write((formatter.reformat(line, idx.getAndIncrement()) + "\n").getBytes(targetEncoding));

            }
            return tempFile;

        }
    }

    private String sourcefilesPatternFormatReader(String sourcefilesPattern) {
        Pattern p = Pattern.compile("(\\{.*\\})");
        Matcher m = p.matcher(sourcefilesPattern);

        if (!m.find()) return sourcefilesPattern;
        String datePattern = m.group(0);
        OffsetDateTime now = OffsetDateTime.now(ZoneId.of("UTC"));
        String formattedDate = now.format(DateTimeFormatter.ofPattern(datePattern.replace("{", "").replace("}", "")));
        return sourcefilesPattern.replace(datePattern, formattedDate);
    }

    private String parseSourceFileName(String[] stringArray)
        throws Exception
    {
        return getLastIndexString(stringArray);
    }

    private String getLastIndexString(String[] stringArray)
        throws Exception
    {
        if (stringArray.length == 0) throw new Exception("zero size array!");
        return stringArray[stringArray.length - 1];
    }

    private String createTargetPath(String cuid, String targetFilePath, String key) {
        if (targetFilePath.length() == 0) return String.join("/", cuid, key);
        return String.join("/", cuid, targetFilePath, key);
    }

}
