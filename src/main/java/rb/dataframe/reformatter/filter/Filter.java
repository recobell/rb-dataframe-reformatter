package rb.dataframe.reformatter.filter;

public interface Filter {
	public boolean doFilter(String line);
}
