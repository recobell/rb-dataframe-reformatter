package rb.dataframe.reformatter.filter;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Filter searchterm string value 
public class GsShopSearchTermFilter
    implements Filter
{
    private Logger logger = LoggerFactory.getLogger(getClass());

    public boolean doFilter(String str) {
        str = str.split(",")[0];
        if (isSingleCharacter(str) || isStartswithBlank(str) || isOverVarchar40(str) || validate(str) || hasEnter(str) || isHexString(str)) return true;
        return false;
    }

    // whether the string value is containing only special characters
    private boolean isOnlySpecialCharacters(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int strlen = str.length();
        Pattern ptrnNonword = Pattern.compile("[^\\w]");
        Matcher mtchr = ptrnNonword.matcher(str);
        int matchcount = 0;
        while (mtchr.find()) {
            matchcount++;
        }
        if (matchcount == strlen) return true;
        return false;
    }

    // whether the string value is a single character
    private boolean isSingleCharacter(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (str.length() < 2) return true;
        return false;
    }

    // whether the string value starts with a whitespace
    private boolean isStartswithBlank(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (Character.isWhitespace(str.charAt(0))) return true;
        return false;
    }

    private boolean isOverVarchar40(String str) {
        return str.length() >= 40 ? true : false;
    }

    private boolean hasEmoji(String str) {

        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Pattern emojiPattern = Pattern.compile("\\p{InEmoticons}");
        Matcher emojiPatternMatcher = emojiPattern.matcher(str);
        if (emojiPatternMatcher.find()) logger.info("Emoji Pattern is found, {}", str);
        return emojiPatternMatcher.find() ? true : false;
    }
    
   
    private boolean validate(String str) {
        
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        str = str.replaceAll("\\s+","");
        
        Pattern validPattern = Pattern.compile("\\p{InBasic_Latin}"
            + "|" +"\\p{InHangul_Jamo}"
            + "|" +"\\p{InHangul_Syllables}"
            + "|" +"\\p{InCJK_Radicals_Supplement}"
            + "|" +"\\p{InCJK_Symbols_and_Punctuation}"
            + "|" +"\\p{InHiragana}"
            + "|" +"\\p{InKatakana}"
            + "|" +"\\p{InHangul_Compatibility_Jamo}"
            + "|" +"\\p{InKatakana_Phonetic_Extensions}"
            + "|" +"\\p{InCJK_Compatibility}"
            + "|" +"\\p{InCJK_Unified_Ideographs_Extension_A}"
            + "|" +"\\p{InCJK_Unified_Ideographs}"
            + "|" +"\\p{InCJK_Compatibility_Ideographs}"
            + "|" +"\\p{InCJK_Compatibility_Forms}"
            + "|" +"\\×"
            + "|" + "\\p{Digit}"
            //+ "|" +"(?:\\d*\\.)?\\d+"
            );
        
        
        Matcher matcher = validPattern.matcher(str);
        int cnt = 0;
        while(matcher.find()){
            cnt++;
        }
        
        if (cnt == str.length()) return false;
        logger.info("{} will be ignored", str);
        return true;
    }
    
    private boolean isHexString(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        boolean isHex = str.contains("\\xF");
        if (isHex) logger.info("{} will be ignored, hexa String", str);
        return isHex;
    }
    

    private boolean hasEnter(String str) {
        str = decodeBase64(str);
        boolean has = str.contains("\n") || str.contains("\r") || str.contains("\"");
        if (has) logger.info("Enter is contained on String {}", str);
        return has;
    }

    private String decodeBase64(String word) {
        word = word.replaceAll("\"\"", "");
        return new String(Base64.getDecoder().decode(word));
    }
}