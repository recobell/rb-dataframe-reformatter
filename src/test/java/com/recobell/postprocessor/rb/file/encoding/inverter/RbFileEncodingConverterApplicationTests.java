package com.recobell.postprocessor.rb.file.encoding.inverter;

import java.io.UnsupportedEncodingException;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import rb.dataframe.reformatter.filter.GsShopSearchTermFilter;

public class RbFileEncodingConverterApplicationTests {

    List<String> strs = new ArrayList<>();

    @Before
    public void init() {
        
        strs.add("원피스");

    }

    public void contextLoads() {
        System.out.println(sourcefilesPatternFormatReader("dmp/{YYYY/MM/dd}/tb_psn_asc_recmd_result.csv.gz"));
    }
    
    
    @Test
    public void filterTest() {
        
        
        for (String s : strs) {
            
            
            s = Base64.getEncoder().encodeToString(s.getBytes());
            
            
            if (isSingleCharacter(s)) System.out.println(s + "  /// a");
            if (isStartswithBlank(s)) System.out.println(s + "  /// b");
            if (isOverVarchar40(s)) System.out.println(s + "  /// c");
            if (validate(s)) System.out.println(s + "  /// d");
            if (isHexString(s)) System.out.println(s + "  /// e");
            System.out.println(s + "  raw");
            
            
            
        }
        
        
    }

    private String sourcefilesPatternFormatReader(String sourcefilesPattern) {
        Pattern p = Pattern.compile("(\\{.*\\})");
        Matcher m = p.matcher(sourcefilesPattern);

        if (!m.find()) return sourcefilesPattern;
        String datePattern = m.group(0);
        OffsetDateTime now = OffsetDateTime.now(ZoneId.of("UTC"));
        String formattedDate = now.format(DateTimeFormatter.ofPattern(datePattern.replace("{", "").replace("}", "")));
        return sourcefilesPattern.replace(datePattern, formattedDate);
    }

    public void strLengthTest() {
        String testString = "ㄴ";
        System.out.println(testString.length());
        System.out.println(testString.getBytes().length);

        testString = "가나다";
        System.out.println(testString.length());
        System.out.println(testString.getBytes().length);

        testString = "abc";
        System.out.println(testString.length());
        System.out.println(testString.getBytes().length);

    }

    public void base64encodeTest() {
        String str = "센텀카이\r";
        String str2 = Base64.getEncoder().encodeToString(str.getBytes());
        System.out.println("7IS87YWA7Lm07J20DQ==");
        System.out.println(str2);
    }

    public void base64decodeTest() {
        String str = "7IS87YWA7Lm07J20DQ==";
        System.out.println("decoed : " + decodeBase64(str));
    }


    // whether the string value is containing only special characters
    private boolean isOnlySpecialCharacters(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int strlen = str.length();
        Pattern ptrnNonword = Pattern.compile("[^\\w]");
        Matcher mtchr = ptrnNonword.matcher(str);
        int matchcount = 0;
        while (mtchr.find()) {
            matchcount++;
        }
        if (matchcount == strlen) return true;
        return false;
    }

    // whether the string value is a single character
    private boolean isSingleCharacter(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (str.length() < 2) return true;
        return false;
    }

    // whether the string value starts with a whitespace
    private boolean isStartswithBlank(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (Character.isWhitespace(str.charAt(0))) return true;
        return false;
    }

    private boolean isOverVarchar40(String str) {
        return str.length() >= 40 ? true : false;
    }

    private boolean hasEmoji(String str) {

        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Pattern emojiPattern = Pattern.compile("\\p{InEmoticons}");
        Matcher emojiPatternMatcher = emojiPattern.matcher(str);
        if (emojiPatternMatcher.find()) System.out.println("Emoji Pattern is found, {}" +  str);
        return emojiPatternMatcher.find() ? true : false;
    }
    
   
    private boolean validate(String str) {
        
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        str = str.replaceAll("\\s+","");
        
        Pattern validPattern = Pattern.compile("\\p{InBasic_Latin}"
            + "|" +"\\p{InHangul_Jamo}"
            + "|" +"\\p{InHangul_Syllables}"
            + "|" +"\\p{InCJK_Radicals_Supplement}"
            + "|" +"\\p{InCJK_Symbols_and_Punctuation}"
            + "|" +"\\p{InHiragana}"
            + "|" +"\\p{InKatakana}"
            + "|" +"\\p{InHangul_Compatibility_Jamo}"
            + "|" +"\\p{InKatakana_Phonetic_Extensions}"
            + "|" +"\\p{InCJK_Compatibility}"
            + "|" +"\\p{InCJK_Unified_Ideographs_Extension_A}"
            + "|" +"\\p{InCJK_Unified_Ideographs}"
            + "|" +"\\p{InCJK_Compatibility_Ideographs}"
            + "|" +"\\p{InCJK_Compatibility_Forms}"
            + "|" +"\\×"
            + "|" + "\\p{Digit}"
            //+ "|" +"(?:\\d*\\.)?\\d+"
            );
        
        
        Matcher matcher = validPattern.matcher(str);
        int cnt = 0;
        while(matcher.find()){
            cnt++;
        }
        
        if (cnt == str.length()) return false;
        System.out.println(str + " will be ignored");
        return true;
    }
    
    private boolean isHexString(String str) {
        str = decodeBase64(str);
        try {
            byte[] strBytes = str.getBytes("UTF-8");
            str = new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        boolean isHex = str.contains("\\xF");
        if (isHex) System.out.println(str + " will be ignored, hexa String");
        return isHex;
    }
    

    private boolean hasEnter(String str) {
        str = decodeBase64(str);
        boolean has = str.contains("\n") || str.contains("\r") || str.contains("\"");
        if (has) System.out.println("Enter is contained on String {}" + str);
        return has;
    }

    private String decodeBase64(String word) {
        word = word.replaceAll("\"\"", "");
        return new String(Base64.getDecoder().decode(word));
    }

}
