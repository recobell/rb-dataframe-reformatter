mvn clean package
aws s3 cp ./target/*.jar s3://rb-rec-postprocessor/rb-dataframe-reformatter/bin/
aws s3 cp ./src/main/resources s3://rb-rec-postprocessor/rb-dataframe-reformatter/conf/ --recursive
